﻿using Helper;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace IocAndDIDemo
{

    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());


            IMailSender mailSender = kernel.Get<IMailSender>();
            FormHandler formHandler = new FormHandler(mailSender);
            formHandler.Handle("deepak.sharma@intimetec.com");
        }
    }
}
