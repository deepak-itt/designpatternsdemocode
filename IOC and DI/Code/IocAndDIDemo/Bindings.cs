﻿using Helper;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IocAndDIDemo
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IMailSender>().To<MockMailSender>();
            Bind<ILogging>().To<ConsoleLogger>();
        }
    }
}
