### What is this repository for? ###

* This repository is created to hold the code for demo targeted to illustrate "Dependency Injection" using Ninject.
* 1.0.0.0

### How do I get set up? ###

* This is VS project, open this in VS 2013, and you are good to go.