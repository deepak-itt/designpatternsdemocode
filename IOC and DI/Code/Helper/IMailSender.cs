﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper
{
   
        public interface IMailSender
        {
            void Send(string toAddress, string subject);
        }
}
