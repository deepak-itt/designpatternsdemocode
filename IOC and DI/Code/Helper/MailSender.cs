﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper
{
    public class MailSender : IMailSender
    {
        private readonly ILogging logger;

        public MailSender(ILogging logger)
        {
            this.logger = logger;
        }

        public void Send(string toAddress, string subject)
        {
            this.logger.Log("Sending Email....");
            Console.WriteLine("Sending mail to [{0}] with subject [{1}]", toAddress, subject);
        }
    }
}
