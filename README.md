
### What is this repository for? ###

* Repo to hold demo code and PPT from TMP meeting.
* 1.0.0.0

### How do I get set up? ###

* There will be folder of every session in main folder "DesignPatternDemoCode". It will again have two folder 
1. Presentation: To put PPT
2. Code: To put code sample.